package com.wraend.inftimetable;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.wraend.inftimetable.database.CoursesTable;

public class SingleCourseActivity extends ActionBarActivity {

	private static final String TAG = "SingleCourseActivity";
	
	private Uri uri;
	private String cAcronym;
	
	private boolean onTimetable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_single_course);
		Bundle extras = getIntent().getExtras();
		
		uri = (savedInstanceState == null) ? null : (Uri) savedInstanceState.getParcelable("com.wraend.inftimetable.course");

		if (extras != null) {
			uri = extras.getParcelable("com.wraend.inftimetable.course");
		}
		
		Log.d(TAG, "Activity passed URI: " + uri);
		fillData(uri);
		
		
        CourseTimetableFragment ctfragment = new CourseTimetableFragment();
        Bundle args = new Bundle();
        args.putString(CourseTimetableFragment.COURSE, cAcronym);
        ctfragment.setArguments(args);
			
        FragmentManager fManager = getSupportFragmentManager();
        FragmentTransaction fTransaction = fManager.beginTransaction();
        fTransaction.add(R.id.course_timetable_frame, ctfragment);
        fTransaction.commit();
     
	}

	private void fillData(Uri uri) {
		String[] projection = { CoursesTable.COLUMN_NAME, CoursesTable.COLUMN_LECTURER, 
								CoursesTable.COLUMN_YEAR, CoursesTable.COLUMN_POINTS,
								CoursesTable.COLUMN_ONTIMETABLE, CoursesTable.COLUMN_ACRONYM };
		Cursor c = getContentResolver().query(uri, projection, null, null, null);
		
		if (c != null) {
			c.moveToFirst();
			String cName = c.getString(c.getColumnIndexOrThrow(CoursesTable.COLUMN_NAME));
			String cLecturer = c.getString(c.getColumnIndexOrThrow(CoursesTable.COLUMN_LECTURER));
			String cYear = String.valueOf(c.getInt(c.getColumnIndexOrThrow(CoursesTable.COLUMN_YEAR)));
			String cPoints = String.valueOf(c.getInt(c.getColumnIndexOrThrow(CoursesTable.COLUMN_POINTS)));
			onTimetable = (c.getInt(c.getColumnIndexOrThrow(CoursesTable.COLUMN_ONTIMETABLE)) == 1);
			cAcronym = c.getString(c.getColumnIndexOrThrow(CoursesTable.COLUMN_ACRONYM));

			
			TextView name = (TextView) findViewById(R.id.course_name);
			TextView lecturer = (TextView) findViewById(R.id.course_lecturer);
			TextView year = (TextView) findViewById(R.id.course_year);
			TextView points = (TextView) findViewById(R.id.course_points);
			//ToggleButton toggle = (ToggleButton) findViewById(R.id.timetable_add_toggle);


			name.setText(cName);
			lecturer.setText(cLecturer);
			year.setText(cYear);
			points.setText(cPoints);
			
		}
		
		c.close();
	}
	
	private void addCourse() {
        ContentValues values = new ContentValues();
        Log.d(TAG, "Course added to timetable");
        values.put(CoursesTable.COLUMN_ONTIMETABLE, 1);
        getContentResolver().update(uri, values, null, null);
	
        // reset options to disable 'add' option
		onTimetable = true;
		this.supportInvalidateOptionsMenu();
	}
	
	private void removeCourse() {
		ContentValues values = new ContentValues();
        Log.d(TAG, "Course removed timetable");
        values.put(CoursesTable.COLUMN_ONTIMETABLE, 0);
        getContentResolver().update(uri, values, null, null);

        // reset options to disable 'remove' option
        onTimetable = false;
		this.supportInvalidateOptionsMenu();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add_course:
			Log.d(TAG, "Add course pressed");
			addCourse();
			return true;
		case R.id.action_remove_course:
			Log.d(TAG, "Remove course pressed");
			removeCourse();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.single_course, menu);	
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (onTimetable) {
			menu.findItem(R.id.action_add_course).setEnabled(false);
			menu.findItem(R.id.action_remove_course).setEnabled(true);
		} else {
			menu.findItem(R.id.action_add_course).setEnabled(true);
			menu.findItem(R.id.action_remove_course).setEnabled(false);
		}
		return true;
	}

}
