package com.wraend.inftimetable;

public interface OnTaskCompleted {
    void onTaskCompleted();
    void onTaskException(Exception e);

}