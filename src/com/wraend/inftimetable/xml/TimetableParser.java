package com.wraend.inftimetable.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;
import android.util.Xml;
import com.wraend.inftimetable.model.TimetableEntry;

/**
 * TimetableParser
 * 
 * Based in part on code examples available from 
 * Android Open Source Project documentation 
 */

final class Venue {
	final String building;
	final String room;
	
	public Venue(String building, String room) {
		this.building = building;
		this.room = room;
	}
}

public class TimetableParser extends XmlParser {
	 private static final String ns  = null;
	 private static final String TAG = "TimetableParser";

		
		public List<TimetableEntry> parse(InputStream in) throws XmlPullParserException, IOException {
	        try {
	            XmlPullParser parser = Xml.newPullParser();
	            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
	            parser.setInput(in, null);
	            parser.nextTag();
	            Log.d(TAG, "Parsing timetable");
	            return readTimetable(parser);
	        } finally {
	            in.close();
	        }
	    }
		
		public List<TimetableEntry> readTimetable(XmlPullParser parser) throws XmlPullParserException, IOException {
			List<TimetableEntry> entries = new ArrayList<TimetableEntry>();
			
			parser.require(XmlPullParser.START_TAG, ns, "timetable");
	        while (parser.next() != XmlPullParser.END_TAG) {
	            if (parser.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            String name = parser.getName();
	            if (name.equals("semester")) {
	            	Log.d(TAG, "Found semester. Adding all entries");
	                entries.addAll(readSemester(parser));
	            } else {
	                skip(parser);
	            }
	        }
	        
			return entries;
		}
		
		private List<TimetableEntry> readSemester(XmlPullParser parser) throws XmlPullParserException, IOException {
	        parser.require(XmlPullParser.START_TAG, ns, "semester");
	        
            int semester = Integer.parseInt(parser.getAttributeValue(ns, "number"));
	        List<TimetableEntry> entries = new ArrayList<TimetableEntry>();
	        
	        while (parser.next() != XmlPullParser.END_TAG) {
	            if (parser.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            String tag = parser.getName();
	            if (tag.equals("week")) {
	                entries.addAll(readWeek(parser, semester));
	            } else {
	                skip(parser);
	            }
	        }
	        return entries;
	    }
		
		private List<TimetableEntry> readWeek(XmlPullParser parser, int semester) throws XmlPullParserException, IOException {
	        parser.require(XmlPullParser.START_TAG, ns, "week");
	        List<TimetableEntry> entries = new ArrayList<TimetableEntry>();
	        
	        while (parser.next() != XmlPullParser.END_TAG) {
	            if (parser.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            String tag = parser.getName();
	            if (tag.equals("day")) {
	                entries.addAll(readDay(parser, semester));
	            } else {
	                skip(parser);
	            }
	        }
	        return entries;
	    }
		
		private List<TimetableEntry> readDay(XmlPullParser parser, int semester) throws XmlPullParserException, IOException {
	        parser.require(XmlPullParser.START_TAG, ns, "day");
	        
	        String dayName = parser.getAttributeValue(ns, "name");
	        List<TimetableEntry> entries = new ArrayList<TimetableEntry>();
	        
	        while (parser.next() != XmlPullParser.END_TAG) {
	            if (parser.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            String tag = parser.getName();
	            if (tag.equals("time")) {
	                entries.addAll(readTime(parser, semester, dayName));
	            } else {
	                skip(parser);
	            }
	        }
	        return entries;
	    }
		
		private List<TimetableEntry> readTime(XmlPullParser parser, int semester, String dayName) throws XmlPullParserException, IOException {
	        parser.require(XmlPullParser.START_TAG, ns, "time");
	        
	        String start = parser.getAttributeValue(ns, "start");
	        String finish = parser.getAttributeValue(ns, "finish");

	        List<TimetableEntry> entries = new ArrayList<TimetableEntry>();
	        
	        while (parser.next() != XmlPullParser.END_TAG) {
	            if (parser.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            String tag = parser.getName();
	            if (tag.equals("lecture")) {
	                entries.add(readLecture(parser, semester, dayName, start, finish));
	            } else {
	                skip(parser);
	            }
	        }
	        return entries;
	    }
		
		private TimetableEntry readLecture(XmlPullParser parser, int semester, String dayName, String startTime, String finishTime) throws XmlPullParserException, IOException {
	        parser.require(XmlPullParser.START_TAG, ns, "lecture");
	        
	        String course = null;
	        List<Integer> years = new ArrayList<Integer>();
	        Venue venue = null;
	        String comment = null;
	        
	        while (parser.next() != XmlPullParser.END_TAG) {
	            if (parser.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            String tag = parser.getName();
	            if (tag.equals("course")) {
	                course = readTagText("course", parser);
	            } else if (tag.equals("years")) {
	            	years.addAll(readYears(parser));
	            } else if (tag.equals("venue")) {
	            	venue = readVenue(parser);
	            } else if (tag.equals("comment") && !tag.isEmpty()) {
	            	comment = readTagText("comment", parser);
	            } else {
	            	skip(parser);
	            }
	        }
	        return new TimetableEntry(course, venue.building, venue.room, semester, dayName, startTime, finishTime, years, comment);
	    }
		
		private List<Integer> readYears(XmlPullParser parser) throws IOException, XmlPullParserException {
	        parser.require(XmlPullParser.START_TAG, ns, "years");
	        List<Integer> years = new ArrayList<Integer>();
	        
	        while (parser.next() != XmlPullParser.END_TAG) {
	            if (parser.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            String tag = parser.getName();
	            if (tag.equals("year")) {
	                years.add(Integer.valueOf(readTagText("year", parser)));
	            } else {
	            	skip(parser);
	            }
	        }
	        
	        return years;
		}
		
		
		private Venue readVenue(XmlPullParser parser) throws IOException, XmlPullParserException {
	        parser.require(XmlPullParser.START_TAG, ns, "venue");
	        
	        String room = null;
	        String building = null;
	        
	        while (parser.next() != XmlPullParser.END_TAG) {
	            if (parser.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            String tag = parser.getName();
	            if (tag.equals("room")) {
	                room = readTagText("room", parser);
	            } else if (tag.equals("building")) {
	            	building = readTagText("building", parser);
	            } else {
	            	skip(parser);
	            }
	        }
	        
	        return new Venue(building,room);
		}		
}
