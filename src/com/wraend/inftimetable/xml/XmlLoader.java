package com.wraend.inftimetable.xml;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;

import com.wraend.inftimetable.model.*;


/**
 * XmlLoader
 * 
 * Based in part on code examples available from 
 * Android Open Source Project documentation 
 */


public class XmlLoader {
	
	private static final String TAG = "XmlLoader";
	
	private String coursesXml = "http://www.inf.ed.ac.uk/teaching/courses/selp/xml/courses.xml";
	private String venuesXml = "http://www.inf.ed.ac.uk/teaching/courses/selp/xml/venues.xml";
	private String timetablesXml = "http://www.inf.ed.ac.uk/teaching/courses/selp/xml/timetable.xml";
	
	public List<Course> loadCourses() throws XmlPullParserException, IOException {
		
		// instantiate parser
	    CoursesParser coursesParser = new CoursesParser();
	    
	    // initialise list to hold courses, InputStream for XML
	    List<Course> courses = null;
	    InputStream stream = null;
	    
	    Log.d(TAG, "Attempting to download and parse courses.xml");
	    
	    // fetch and parse courses.xml
	    try {
	        stream = downloadUrl(coursesXml);        
	        courses = coursesParser.parse(stream);
	    } finally {
	        if (stream != null) {
	            stream.close();
	        } 
	    }
	    
	    Log.d(TAG, "Found " + courses.size() + " courses");
	    return courses;
	}
		
	public VenueCollection loadVenues() throws XmlPullParserException, IOException {
			
		// instantiate parser
	    VenuesParser venuesParser = new VenuesParser();
	    
	    // initialise VenueCollection to hold venues, InputStream for XML
	    VenueCollection venues = null;
	    InputStream stream = null;
	    
	    Log.d(TAG, "Attempting to download and parse venues.xml");
	    
	    // fetch and parse venues.xml
	    try {
	        stream = downloadUrl(venuesXml);        
	        venues = venuesParser.parse(stream);
	    } finally {
	        if (stream != null) {
	            stream.close();
	        } 
	    }
	    
	    Log.d(TAG, "Found " + venues.buildings.size() + " buildings and " + venues.rooms.size() + " rooms");
	    return venues;
	}
	
	public List<TimetableEntry> loadTimetable() throws XmlPullParserException, IOException {
		// instantiate parser
	    TimetableParser timetableParser = new TimetableParser();
	    
	    // initialise VenueCollection to hold venues, InputStream for XML
	    List<TimetableEntry> entries = null;
	    InputStream stream = null;
	    
	    Log.d(TAG, "Attempting to download and parse timetables.xml");
	    
	    // fetch and parse courses.xml
	    try {
	        stream = downloadUrl(timetablesXml);        
	        entries = timetableParser.parse(stream);
	    } finally {
	        if (stream != null) {
	            stream.close();
	        } 
	    }
	    
	    Log.d(TAG, "Found " + entries.size() + " timetable entries");
	    
	    return entries;
	}
	
	private InputStream downloadUrl(String urlString) throws IOException {
	    URL url = new URL(urlString);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setReadTimeout(10000 /* milliseconds */);
	    conn.setConnectTimeout(15000 /* milliseconds */);
	    conn.setRequestMethod("GET");
	    conn.setDoInput(true);
	    // Starts the query
	    conn.connect();
	    return conn.getInputStream();
	}
}
