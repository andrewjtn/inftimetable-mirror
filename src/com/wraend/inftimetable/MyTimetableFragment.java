package com.wraend.inftimetable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.wraend.inftimetable.database.CoursesTable;
import com.wraend.inftimetable.database.TimetableTable;
import com.wraend.inftimetable.model.TimetableEntry;
import com.wraend.inftimetable.provider.TimetableContentProvider;

public class MyTimetableFragment extends Fragment {
	
	private static final String TAG = "MyTimetableFragment";
	
	private ExpandableListView ttList;
	private RelativeLayout ttProgress;
	private LinearLayout ttEmpty;
	
	private MyTimetableAdapter ttAdapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View V = inflater.inflate(R.layout.fragment_mytimetable, container, false);
		ttList = (ExpandableListView) V.findViewById(android.R.id.list);
		ttProgress = (RelativeLayout) V.findViewById(R.id.progress_layout);
		ttEmpty = (LinearLayout) V.findViewById(android.R.id.empty);
        return V;    
    }

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		ttEmpty.setVisibility(8);
		ttProgress.setVisibility(0);
	
		String[] proj = { CoursesTable.COLUMN_ACRONYM };
		Cursor c = getActivity().getContentResolver().query(TimetableContentProvider.COURSES_URI, 
															proj, CoursesTable.COLUMN_ONTIMETABLE + "=1", 
															null, null);
		
		if (!(c.moveToFirst()) || c == null) {
			Log.d(TAG, "No courses on timetable");
			ttEmpty.setVisibility(0);
			ttProgress.setVisibility(8);			
		}
		else {
			
			String whereClause = "(" + TimetableTable.COLUMN_COURSE + "='" + c.getString(0) + "'";
			c.moveToNext();
			
			while (c.isAfterLast() == false) {
				whereClause += " OR " + TimetableTable.COLUMN_COURSE + "='" + c.getString(0) + "'";
				c.moveToNext();
			}
			
			whereClause += ")";
			Log.d(TAG, whereClause);
			
			Cursor entries = getActivity().getContentResolver().query(TimetableContentProvider.TIMETABLE_URI,
																		null, whereClause, null, null);
			
			selectCourses(entries);
		}	
	}
	
	public void selectCourses(Cursor c) {
		List<String> listDataHeader = new ArrayList<String>();
		HashMap<String, List<TimetableEntry>> listDataChild = new HashMap<String, List<TimetableEntry>>();

		c.moveToFirst();
		
		while (c.isAfterLast() == false) {
			
			String cAcronym = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_COURSE));
			String dayOfWeek = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_DAYOFWEEK));
			String startTime = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_STARTTIME));
			String finishTime = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_FINISHTIME));
			String room = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_ROOM));
			String building = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_BUILDING));
			Integer semester = c.getInt(c.getColumnIndexOrThrow(TimetableTable.COLUMN_SEMESTER));
			String comment = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_COMMENT));
			
			TimetableEntry entry = new TimetableEntry(cAcronym, building, room, semester, dayOfWeek,
														startTime, finishTime, null, comment);

			// check if this lecture is on a day which we don't yet know
			if (listDataChild.get(dayOfWeek) == null) {
				// create empty list for this day of week and add the current lecture
				List<TimetableEntry> list = new ArrayList<TimetableEntry>();
				list.add(entry);
				
				// add this day of week to the data
				listDataChild.put(dayOfWeek, list);
			} else {
				// get this day of week's list of lectures and add this lecture
				List<TimetableEntry> list = listDataChild.get(dayOfWeek);
				list.add(entry);
				
				// replace this day of week's data
				listDataChild.put(dayOfWeek, list);				
			}
		
			Log.d(TAG, "Lecture starting on " + c.getString(2) + " at " + c.getString(0) + " and ending at " + c.getString(1));
			c.moveToNext();
		}
		
		// add each day which has lectures as a header
		for (String s : listDataChild.keySet()) {
			listDataHeader.add(s);
			List<TimetableEntry> entries = listDataChild.get(s);
			Collections.sort(entries, TIME_OF_DAY_ORDER);
			listDataChild.put(s, entries);
		}
		
		Collections.sort(listDataHeader, DAY_OF_WEEK_ORDER);
		ttAdapter = new MyTimetableAdapter(getActivity(), listDataHeader, listDataChild);
		ttList.setAdapter(ttAdapter);
		ttEmpty.setVisibility(8);
		ttProgress.setVisibility(8);
		ttList.setVisibility(0);
		
		int count = ttAdapter.getGroupCount();
		for (int position = 1; position <= count; position++) {
		    ttList.expandGroup(position - 1);
		}

		//Log.d(TAG, "Going to try finding " + TimetableTable.COLUMN_COURSE + " = "+ courseAcronym);
		
		//String[] projection = { CoursesTable.COLUMN_ACRONYM };

		//resolver.query(TimetableContentProvider.COURSES_URI, projection, 
									
		/*while (c.isAfterLast() == false) {
			Log.d(TAG, "Lecture starts at " + c.getString(0) + " and ends at " + c.getString(1));
			c.moveToNext();
		}*/
		
	}
	
	 static final Comparator<String> DAY_OF_WEEK_ORDER = 
             new Comparator<String>() {		
 		public int compare(String day1, String day2) {
 			HashMap<String,Integer> days = new HashMap<String,Integer>();
 			days.put("Monday", 0);
 			days.put("Tuesday", 1);
 			days.put("Wednesday", 2);
 			days.put("Thursday", 3);
 			days.put("Friday", 4);
 			
 			if (days.get(day1) == null) {
 				days.put(day1, 5);
 			}
 			
 			if (days.get(day2) == null) {
 				days.put(day2, 6);
 			}
 			
 			return days.get(day1).compareTo(days.get(day2));
   		}
 		
	 };
	 
	 static final Comparator<TimetableEntry> TIME_OF_DAY_ORDER = 
             new Comparator<TimetableEntry>() {		
 		public int compare(TimetableEntry entry1, TimetableEntry entry2) {
 		
 			// compare the hours of two start times
 			// comparison doesn't need to be better than this under the common university timetable
 			Integer startHour1 = Integer.parseInt(entry1.startTime.substring(0, 2));
 			Integer startHour2 = Integer.parseInt(entry2.startTime.substring(0, 2));

 			return startHour1.compareTo(startHour2);
   		}
 		
	 };
	
	@Override
	public void onStart() {
		super.onStart();
		/*Context context = getActivity();
		CharSequence text = "Hello toast!";
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();*/
	}
	
}