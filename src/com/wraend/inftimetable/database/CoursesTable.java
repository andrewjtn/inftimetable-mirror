package com.wraend.inftimetable.database;


public class CoursesTable {

	public static final String TAG = "CoursesHelper";
	
	public static final String NAME = "courses";
	
	// name columns
	public final static String COLUMN_ID = "_id";
	public final static String COLUMN_ACRONYM = "acronym";
	public final static String COLUMN_NAME =  "name";
	public final static String COLUMN_URL = "url";
	public final static String COLUMN_LECTURER = "lecturer";
	public final static String COLUMN_YEAR = "year";
	public final static String COLUMN_LEVEL = "level";
	public final static String COLUMN_POINTS = "points";
	public final static String COLUMN_DELIVERYPERIOD = "deliveryperiod";
	public final static String COLUMN_DRPS = "drps";
	public final static String COLUMN_EUCLID = "euclid";
	public final static String COLUMN_CS = "cs";
	public final static String COLUMN_SE = "se";
	public final static String COLUMN_AI = "ai";
	public final static String COLUMN_CG = "cg";
	public final static String COLUMN_ONTIMETABLE = "ontimetable";
	
	// SQL statement to create 'courses' table
	public static final String CREATE_TABLE = 
			"CREATE TABLE " + NAME + " "
			+ "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ COLUMN_ACRONYM + " TEXT UNIQUE,"
			+ COLUMN_NAME + " TEXT NOT NULL,"
			+ COLUMN_URL + " TEXT,"
			+ COLUMN_LECTURER + " TEXT,"
			+ COLUMN_YEAR + " INTEGER,"
			+ COLUMN_LEVEL + " INTEGER,"
			+ COLUMN_POINTS + " INTEGER,"
			+ COLUMN_DELIVERYPERIOD + " TEXT,"
			+ COLUMN_DRPS + " TEXT,"
			+ COLUMN_EUCLID + " TEXT,"
			+ COLUMN_CS + " INTEGER,"
			+ COLUMN_SE + " INTEGER,"
			+ COLUMN_AI + " INTEGER,"
			+ COLUMN_CG + " INTEGER,"
			+ COLUMN_ONTIMETABLE + " INTEGER)";	

}
