package com.wraend.inftimetable.database;

public class BuildingsTable {

	public static final String TAG = "BuildingsHelper";
	
	public static final String NAME = "buildings";
	
	// name columns
	public static final String COLUMN_NAME = "_id";
	public static final String COLUMN_DESCRIPTION = "description";
	public static final String COLUMN_MAPURL = "mapurl";
	
	public static final String CREATE_TABLE =
			"CREATE TABLE " + NAME + " "
			+ "(" + COLUMN_NAME + " TEXT PRIMARY KEY,"
			+ COLUMN_DESCRIPTION + " TEXT NOT NULL,"
			+ COLUMN_MAPURL + " TEXT)";

}
