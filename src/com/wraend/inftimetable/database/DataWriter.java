package com.wraend.inftimetable.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.wraend.inftimetable.model.Building;
import com.wraend.inftimetable.model.Course;
import com.wraend.inftimetable.model.Room;
import com.wraend.inftimetable.model.TimetableEntry;
import com.wraend.inftimetable.provider.TimetableContentProvider;

public class DataWriter {
	private static String TAG = "DataWriter";
	
	private Context context;
	private ContentResolver resolver;
	
	public DataWriter(Context context) {
		this.context = context;
		this.resolver = context.getContentResolver();
	}
	
	public void clearAll() {
		Log.d(TAG, "Will now clear database");
		DBHelper dbHelper = new DBHelper(context);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		dbHelper.onUpgrade(db, 0, 1);
		db.close();
	}
	
	public void addCourse(Course c) throws SQLException {
		
		ContentValues values = new ContentValues();
		values.put(CoursesTable.COLUMN_NAME, c.name);
		values.put(CoursesTable.COLUMN_URL, c.url);
		values.put(CoursesTable.COLUMN_ACRONYM, c.acronym);
		values.put(CoursesTable.COLUMN_LECTURER, c.lecturer);
		values.put(CoursesTable.COLUMN_YEAR, c.year);
		values.put(CoursesTable.COLUMN_LEVEL, c.level);
		values.put(CoursesTable.COLUMN_POINTS, c.points);
		values.put(CoursesTable.COLUMN_DELIVERYPERIOD, c.deliveryperiod);
		values.put(CoursesTable.COLUMN_DRPS, c.drps);
		values.put(CoursesTable.COLUMN_EUCLID, c.euclid);
		values.put(CoursesTable.COLUMN_CS, c.cs);
		values.put(CoursesTable.COLUMN_SE, c.se);
		values.put(CoursesTable.COLUMN_AI, c.ai);
		values.put(CoursesTable.COLUMN_CG, c.cg);
		values.put(CoursesTable.COLUMN_ONTIMETABLE, 0);
		
		resolver.insert(TimetableContentProvider.COURSES_URI, values);

	}
	
	public void addBuilding(Building b) {
		ContentValues values = new ContentValues();
		
		values.put(BuildingsTable.COLUMN_NAME, b.name);
		values.put(BuildingsTable.COLUMN_DESCRIPTION, b.description);
		values.put(BuildingsTable.COLUMN_MAPURL, b.mapUrl);
		
		resolver.insert(TimetableContentProvider.BUILDINGS_URI, values);
	}
	
	public void addRoom(Room r) {
		ContentValues values = new ContentValues();
		
		values.put(RoomsTable.COLUMN_NAME, r.name);
		values.put(RoomsTable.COLUMN_DESCRIPTION, r.description);
		
		resolver.insert(TimetableContentProvider.ROOMS_URI, values);
	}
	
	public void addTimetableEntry(TimetableEntry t) {
		ContentValues values = new ContentValues();
		
		values.put(TimetableTable.COLUMN_COURSE, t.course);
		values.put(TimetableTable.COLUMN_BUILDING, t.building);
		values.put(TimetableTable.COLUMN_ROOM, t.room);
		values.put(TimetableTable.COLUMN_SEMESTER, t.semester);
		values.put(TimetableTable.COLUMN_DAYOFWEEK, t.dayOfWeek);
		values.put(TimetableTable.COLUMN_STARTTIME, t.startTime);
		values.put(TimetableTable.COLUMN_FINISHTIME, t.finishTime);
		values.put(TimetableTable.COLUMN_COMMENT, t.comment);
		
		resolver.insert(TimetableContentProvider.TIMETABLE_URI, values);
	}


}
