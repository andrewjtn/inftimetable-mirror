package com.wraend.inftimetable.model;

public class Course {
    public final String name;
    public final String url;
    public final String acronym;
    public final String lecturer;
    public final Integer year;
    public final Integer level;
    public final Integer points;
    public final String deliveryperiod;
    public final String drps;
    public final String euclid;
    public final boolean cs;
    public final boolean se;
    public final boolean ai;
    public final boolean cg;
    
    
    public Course(String name, String url, String acronym, String lecturer, Integer year, Integer level, Integer points, String deliveryperiod, String drps, String euclid, boolean cs, boolean se, boolean ai, boolean cg) {
        this.name = name;
        this.url = url;
        this.acronym = acronym;
        this.lecturer = lecturer;
        this.year = year;
        this.level = level;
        this.points = points;
        this.deliveryperiod = deliveryperiod;
        this.drps = drps;
        this.euclid = euclid;
        this.cs = cs;
        this.se = se;
        this.ai = ai;
        this.cg = cg;
    }
}