package com.wraend.inftimetable.model;

public class Room {
    public final String name;
    public final String description;

    public Room(String name, String description) {
        this.name = name;
        this.description = description;
    }
}