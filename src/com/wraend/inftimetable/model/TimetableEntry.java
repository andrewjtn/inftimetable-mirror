package com.wraend.inftimetable.model;

import java.util.List;

public class TimetableEntry {
	public final String course;
	public final String building;
	public final String room;
	public final Integer semester;
	public final String dayOfWeek;
	public final String startTime;
	public final String finishTime;
	public final List<Integer> years;
	public final String comment;
	
	public TimetableEntry(String course, String building, String room, Integer semester, String dayOfWeek, String startTime, String finishTime, List<Integer> years, String comment) {
		this.course = course;
		this.building = building;
		this.room = room;
		this.semester = semester;
		this.dayOfWeek = dayOfWeek;
		this.startTime = startTime;
		this.finishTime = finishTime;
		this.years = years;
		this.comment = comment;
	}
}