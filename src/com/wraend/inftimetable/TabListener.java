package com.wraend.inftimetable;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;

public class TabListener<T extends Fragment> implements ActionBar.TabListener {
	
	private Fragment mFragment;
    private final Activity mActivity;
    private final String mTag;
    private final Class<T> mClass;
    
    public TabListener(Activity activity, String tag, Class<T> c) {
        mActivity = activity;
        mTag = tag;
        mClass = c;
    }
    
	@Override
	public void onTabReselected(Tab t, FragmentTransaction ft) {
		// do nothing	
	}

	@Override
	public void onTabSelected(Tab t, FragmentTransaction ft) {
		// Check if the fragment is already initialized
        if (mFragment == null) {
            // If not, instantiate and add it to the activity
            mFragment = Fragment.instantiate(mActivity, mClass.getName());
            ft.add(android.R.id.content, mFragment, mTag);
        } else {
            // If it exists, simply attach it in order to show it
            ft.attach(mFragment);
        }
	}

	@Override
	public void onTabUnselected(Tab t, FragmentTransaction ft) {
		if (mFragment != null) {
            // Detach the fragment, because another one is being attached
            ft.detach(mFragment);
        }		
	}

}
